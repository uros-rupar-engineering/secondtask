//
//  ViewController.swift
//  secondTask
//
//  Created by uros.rupar on 5/11/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        login.layer.cornerRadius = 15
        
        fb.layer.cornerRadius = 15
        
        goog.layer.cornerRadius = 15
        usernameTextField.borderStyle = .none
        passwordTextField.borderStyle = .none
    }
    
    
    @IBOutlet weak var login: UIButton!
    

    @IBOutlet weak var usernameTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var fb: UIButton!
    @IBOutlet weak var goog: UIButton!
    
    @IBOutlet weak var username: UITextField!
}

